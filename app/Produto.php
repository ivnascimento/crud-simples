<?php

namespace App;

use App\Categoria;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    const created_at = 'tempo_criacao';
    const updated_at = 'tempo_alteracao';

    protected $primaryKey = 'PRO_ID';

    protected $fillable = [
        'PRO_NOME', 'PRO_IDCATEGORIA', 'PRO_DESCRICAO'
    ];

    public function categoria()
    {
        return $this->hasOne('App\Categoria', 'CTG_ID', 'PRO_IDCATEGORIA');
    }
}
