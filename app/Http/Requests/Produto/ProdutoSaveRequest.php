<?php

namespace App\Http\Requests\Produto;

class ProdutoSaveRequest extends ProdutoRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PRO_NOME' => 'required|max:255',
            'PRO_IDCATEGORIA' => 'required|max:255',
            'PRO_DESCRICAO' => 'required|max:255',
        ];
    }
}
