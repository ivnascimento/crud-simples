<?php

namespace App\Http\Requests\Categoria;

class CategoriaEditRequest extends CategoriaRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'CTG_NOME' => 'required|max:255'
        ];
    }
}
