<?php

namespace App\Http\Controllers;

use App\Produto;
use App\Categoria;
use Illuminate\Http\Request;
use App\Http\Requests\Produto\ProdutoSaveRequest;
use App\Http\Requests\Produto\ProdutoEditRequest;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::all();

        return view('produto.listar', compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = ['' => 'Selecione Categoria'];
        $_categorias = Categoria::all();
        foreach($_categorias as $categoria){
          $categorias[$categoria->CTG_ID] = $categoria->CTG_NOME;
        }

        return view('produto.criar', compact('categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoSaveRequest $request)
    {
        $dataForm = $request->all();

    		Produto::create($dataForm);

        return redirect('/produto')->with('success', 'Produto adicionado com sucesso.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produto = Produto::find($id);

        $categorias = ['' => 'Selecione Categoria'];
        $_categorias = Categoria::all();
        foreach($_categorias as $categoria){
          $categorias[$categoria->CTG_ID] = $categoria->CTG_NOME;
        }

        return view('produto.editar', compact('produto', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoEditRequest $request, $id)
    {
        $dataForm = $request->all();

        $produto = Produto::find($id);

        $produto->update($dataForm);

        return redirect('/produto')->with('success', 'Produto atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Produto::destroy($id);

      return redirect('/produto')->with('success', 'Produto deletado com sucesso.');
    }
}
