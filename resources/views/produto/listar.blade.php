@extends('layouts.app')

@section('title', 'Listar Produtos')

@section('produto', 'active')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="text-primary">Listar Produtos</h3>
        <p><a href="{{ route('produto.create') }}" class="btn btn-primary">Novo</a></p>

        <table id="table" class="table table-responsive table-bordered table-hover powertabela">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Categoria</th>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($produtos as $pro)
                <tr>
                    <td>{{ $pro->PRO_ID  }}</td>
                    <td>{{ $pro->PRO_NOME }}</td>
                    <td>{{ $pro->categoria->CTG_NOME }}</td>
                    <td>{{ $pro->PRO_DESCRICAO }}</td>
                    <td>

                        <?php // Botão editar ?>
                        <div class="btn-group">
                            <a href="{{ route('produto.edit', ['pro' => $pro->PRO_ID]) }}" type="button" class="btn btn-warning btn-flat"><i class="fa fa-pencil"></i></a>
                        </div>

                        <?php // Botão deletar?>
                        <div class="btn-group">
                            <?php $deleteForm = "delete-form-{$loop->index}"; ?>
                            <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#delete_pro_ID{{$pro->PRO_ID}}"><i class="fa fa-trash-o"></i></a>

                            <?php// modal deletar ?>
                            <div class="modal fade" id="delete_pro_ID{{$pro->PRO_ID }}" tabindex="-1" role="dialog" aria-labelledby="delete_pro_ID{{$pro->PRO_ID}}Label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title text-primary" id="delete_pro_ID{{$pro->PRO_ID}}Label"><i class="fa fa-trash-o"></i> Deletar Produto</h4>
                                        </div>
                                        <div class="modal-body">

                                            <p class="alert alert-danger">Tem certeza que deseja excluir produto "{{ $pro->PRO_NOME }}" ?</p>
                                            <div class="form-actions">
                                                <a href="{{ route('produto.destroy', ['pro' => $pro->PRO_ID]) }}" onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();" class="btn btn-danger btn-flat">SIM</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">NÃO</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            {!! Form::open(['route' => ['produto.destroy', 'pro' => $pro->PRO_ID], 'method' => 'DELETE', 'id' => $deleteForm, 'style' => 'display:none']) !!}
                            {!! Form::close() !!}

                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@stop
