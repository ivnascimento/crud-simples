@extends('layouts.app')

@section('title', 'Adicionar Produto')

@section('produto', 'active')

@section('content')

{!! Form::open(['route' => 'produto.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-md-12">
        <h3 class="text-primary">Adicionar Produto</h3>
    </div>

    <div class="col-md-8">
        <div class='row'>

            <div class='col-md-6'>

                <?php // Nome ?>
                <div class='form-group'>
                    {{ Form::label('PRO_NOME', 'Nome:', ['data-error' => $errors->first('PRO_NOME')]) }}
                    {{ Form::text('PRO_NOME', null, ['class' => 'avalidate form-control', 'autocomplete' => 'off']) }}

                    @if ($errors->has('PRO_NOME'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('PRO_NOME') }}</strong>
                    </span>
                    @endif
                </div>

                <?php // Descrição ?>
                <div class='form-group'>
                    {{ Form::label('PRO_DESCRICAO', 'Descrição:', ['data-error' => $errors->first('PRO_DESCRICAO')]) }}
                    {{ Form::text('PRO_DESCRICAO', null, ['class' => 'avalidate form-control', 'autocomplete' => 'off']) }}

                    @if ($errors->has('PRO_DESCRICAO'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('PRO_DESCRICAO') }}</strong>
                    </span>
                    @endif
                </div>

            </div>

            <div class="col-md-6">
                <?php // Categoria ?>
                <div class='form-group'>
                    {{ Form::label('PRO_IDCATEGORIA', 'Categoria:', ['data-error' => $errors->first('PRO_CATEGORIA')]) }}
                    {{ Form::select('PRO_IDCATEGORIA', $categorias, null, ['class' => 'avalidate form-control chosen-select-UNI_IDIMOVEL', 'autocomplete' => 'off']) }}



                    @if ($errors->has('PRO_IDCATEGORIA'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('PRO_IDCATEGORIA') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-4">
        <button type="submit" type="button" class="btn btn-block btn-success"><i class="fa fa-floppy-o"></i> Salvar cadastro</button>
        <button onclick="history.back()" type="button" class="btn btn-block btn-danger"><i class="fa fa-close"></i> Cancelar</button>
    </div>

</div>
{!! Form::close() !!}

@stop
