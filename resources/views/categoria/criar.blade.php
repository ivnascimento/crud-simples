@extends('layouts.app')

@section('title', 'Adicionar Categoria')

@section('categoria', 'active')

@section('content')

{!! Form::open(['route' => 'categoria.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-md-12">
        <h3 class="text-primary">Adicionar Categoria</h3>
    </div>

    <div class="col-md-8">
        <div class='form-group'>
            {{ Form::label('CTG_NOME', 'Nome:', ['data-error' => $errors->first('CTG_NOME')]) }}
            {{ Form::text('CTG_NOME', null, ['class' => 'avalidate form-control', 'autocomplete' => 'off']) }}

            @if ($errors->has('CTG_NOME'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('CTG_NOME') }}</strong>
            </span>
            @endif
        </div>
    </div>

    <div class="col-md-4">
        <button type="submit" type="button" class="btn btn-block btn-success"><i class="fa fa-floppy-o"></i> Salvar cadastro</button>
        <button onclick="history.back()" type="button" class="btn btn-block btn-danger"><i class="fa fa-close"></i> Cancelar</button>
    </div>

</div>
{!! Form::close() !!}

@stop
