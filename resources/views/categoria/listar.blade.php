@extends('layouts.app')

@section('title', 'Listar Categorias')

@section('categoria', 'active')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3 class="text-primary">Listar Categorias</h3>
        <p><a href="{{ route('categoria.create') }}" class="btn btn-primary">Novo</a></p>

        <table id="table" class="table table-responsive table-bordered table-hover powertabela">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categorias as $ctg)
                <tr>
                    <td>{{ $ctg->CTG_ID  }}</td>
                    <td>{{ $ctg->CTG_NOME }}</td>
                    <td>

                        <?php // Botão editar ?>
                        <div class="btn-group">
                            <a href="{{ route('categoria.edit', ['ctg' => $ctg->CTG_ID]) }}" type="button" class="btn btn-warning btn-flat"><i class="fa fa-pencil"></i></a>
                        </div>

                        <?php // Botão deletar?>
                        <div class="btn-group">
                            <?php $deleteForm = "delete-form-{$loop->index}"; ?>
                            <a class="btn btn-danger btn-flat" data-toggle="modal" data-target="#delete_ctg_ID{{$ctg->CTG_ID}}"><i class="fa fa-trash-o"></i></a>

                            <?php// modal deletar ?>
                            <div class="modal fade" id="delete_ctg_ID{{$ctg->CTG_ID }}" tabindex="-1" role="dialog" aria-labelledby="delete_ctg_ID{{$ctg->CTG_ID}}Label" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title text-primary" id="delete_ctg_ID{{$ctg->CTG_ID}}Label"><i class="fa fa-trash-o"></i> Deletar Categoria</h4>
                                        </div>
                                        <div class="modal-body">

                                            <p class="alert alert-danger">Tem certeza que deseja excluir categoria "{{ $ctg->CTG_NOME }}" ?</p>
                                            <div class="form-actions">
                                                <a href="{{ route('categoria.destroy', ['ctg' => $ctg->CTG_ID]) }}" onclick="event.preventDefault(); document.getElementById('{{$deleteForm}}').submit();" class="btn btn-danger btn-flat">SIM</a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">NÃO</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            {!! Form::open(['route' => ['categoria.destroy', 'ctg' => $ctg->CTG_ID], 'method' => 'DELETE', 'id' => $deleteForm, 'style' => 'display:none']) !!}
                            {!! Form::close() !!}

                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
@stop
