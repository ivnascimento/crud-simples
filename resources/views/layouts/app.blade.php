<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="_token" content="{{csrf_token()}}" />
        <title>@yield('title')</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('vendor/Ionicons/css/ionicons.min.css') }}">

        <!-- Chosen -->
        <link rel="stylesheet" href="{{ asset('vendor/chosen/chosen.css') }}">

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">

        <link rel="stylesheet" href="//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.css">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Gugi" rel="stylesheet">
    </head>

    <body>

      <div class="container">

          <ul class="nav nav-tabs">
              <li role="presentation" class="@yield('produto')"><a href="/produto">Produtos</a></li>
              <li role="presentation" class="@yield('categoria')" ><a href="/categoria">Categorias</a></li>
          </ul>

          @if(session()->has('success'))
          <div style="padding-top: 30px">
              <div class="alert alert-success">
                  {{ session()->get('success') }}
              </div>
          </div>
          @endif

          @yield('content')
      </div>

      <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
      <script src="{{ asset('vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
      <script src="{{ asset('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

      <script src="{{ asset('js/jquery-inputmask/dist/jquery.inputmask.bundle.js') }}"></script>

      <!-- Chosen -->
      <script src="{{ asset('vendor/adminlte/vendor/chosen/chosen.jquery.js') }}"></script>

      <!-- Select2 -->
      <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

      <!-- DataTables with bootstrap 3 renderer -->
      <script src="//cdn.datatables.net/v/bs/dt-1.10.18/datatables.min.js"></script>

      <!-- ChartJS -->
      <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js"></script>

      <script type="text/javascript">
        $(document).ready( function () {
          $('#table').DataTable();
        } );
      </script>

    </body>
</html>
