<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->increments('PRO_ID');

            $table->string('PRO_NOME', 200);

            $table->integer('PRO_IDCATEGORIA')->unsigned();
            $table->foreign('PRO_IDCATEGORIA')->references('CTG_ID')->on('categorias')->onDelete('cascade');

            $table->string('PRO_DESCRICAO', 200);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
